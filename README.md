## E3mel Business Task
-  [session](https://laravel.com/docs/session) 
first to run this application you need to make the following steps
   - got to project directory and run :  [composer install]()
   - create database name=[task]()
   - next copy [.env.example]() and rename it to [.env]()
   - put the credentials of database in username and password in the env file
   - next run : [php artisan migrate:fresh --seed]()
   - next import postman.json file or link to your postman
   - next run : [php artisan serve --port=8000]()
   - finally, you can test apis attached in postman file and read the readme file of the front-end
