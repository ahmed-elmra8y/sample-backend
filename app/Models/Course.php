<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use HasFactory, SoftDeletes;

    public const  courseLevels = [
        'BEGINNER' => 'beginner',
        'IMMEDIATE' => 'immediate',
        'HIGH' => 'high'
    ];
    protected $with = [
        'category'
    ];
    protected $fillable = [
        'name',
        'description',
        'rating',
        'views',
        'hours',
        'is_active',
        'levels',
        'category_id',
    ];
    protected $casts = [
        'is_active' => 'bool'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
