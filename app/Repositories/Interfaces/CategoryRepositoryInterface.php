<?php

namespace App\Repositories\Interfaces;

use App\Models\Category;


interface CategoryRepositoryInterface
{
    public function getAllCategories();

    public function getCategories($perPage);

    public function create(Category $category);

    public function show($id);

    public function delete($id);
}
