<?php

namespace App\Repositories;

use App\Models\Course;
use App\Repositories\Interfaces\CourseRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class CourseRepository implements CourseRepositoryInterface
{
    public function getAllCourses()
    {
        return Course::query()->latest()->get();
    }

    public function getCourses($perPage)
    {
        return Course::query()->where('is_active', true)->latest()->paginate($perPage);
    }

    public function create(Course $course)
    {
        $course->save();
        return $course->refresh();
    }

    public function show($id)
    {
        return Course::query()->find($id);
    }

    public function edit($id, $data)
    {
        $course = $this->show($id);
        $course->update($data);
        return $course->refresh();
    }

    public function delete($id)
    {
        $course = $this->show($id);
        return $course->delete();
    }

    public function search($filters)
    {
        return Course::query()
            ->where('name', 'like', '%' . $filters['key'] . '%')
            ->orWhere('description', 'like', '%' . $filters['key'] . '%')->get();
    }
}
