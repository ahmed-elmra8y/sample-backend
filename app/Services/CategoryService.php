<?php


namespace App\Services;


use App\Models\Category;
use App\Models\Course;
use App\Repositories\CategoryRepository;
use App\Repositories\CourseRepository;
use Illuminate\Database\Eloquent\Model;

class CategoryService
{
    public $categoryRepository;

    public function __construct(CategoryRepository $courseRepository)
    {
        $this->categoryRepository = $courseRepository;
    }

    public function getAllCategories()
    {
        return $this->categoryRepository->getAllCategories();
    }

    public function getActiveCategory($perPage)
    {
        return $this->categoryRepository->getCategories($perPage);
    }

    public function createCategory($data)
    {
        $course = new Category();
        $course->fill($data);
        return $this->categoryRepository->create($course);
    }

    public function categoryDetails($id)
    {
        return $this->categoryRepository->show($id);
    }

    public function updateCategory($id, $data)
    {
        return $this->categoryRepository->edit($id, $data);
    }


    public function softDeleteCategory($id)
    {
        return $this->categoryRepository->delete($id);
    }
    public function search($filters)
    {
        return $this->categoryRepository->search($filters);
    }
}






