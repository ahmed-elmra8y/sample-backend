<?php

namespace App\Http\Middleware;

use App\Exceptions\Custom\AuthorizationException;
use Closure;
use Illuminate\Contracts\Auth\Authenticatable;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        $user = \app()->get(Authenticatable::class);
        throw_if((get_class($user) != Admin::class), new AuthorizationException('unauthorized user'));
        return $next($request);
    }
}
