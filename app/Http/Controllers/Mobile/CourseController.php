<?php

namespace App\Http\Controllers\Mobile;

use App\Helpers\Facades\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\CourseResource;
use App\Services\CourseService;
use Illuminate\Http\Request;


class CourseController extends Controller
{
    public $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function activeCourses(Request $request)
    {
        $perPage = $request->per_page ?? 5;
        $courses = $this->courseService->getActiveCourses($perPage);
        return ApiResponse::success(200, CourseResource::collection($courses), 'Success', $courses->toArray());
    }
}
