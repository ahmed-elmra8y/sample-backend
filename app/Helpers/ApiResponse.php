<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Facade;

class ApiResponse extends Facade
{
    public function success($code = 200, $data = [], $message = 'Successful', $pagination = null)
    {
        $pagination = $pagination !== null ? Pagination::paginate($pagination) : null;
        return response()->json([
            'data' => $data,
            'message' => $message,
            'pagination' => $pagination,
        ], $code);
    }

    public function error($code = 400, $errors = [], $message = '')
    {
        return response()->json([
            'message' => $message,
            'errors' => $errors,
        ], $code);
    }
}
