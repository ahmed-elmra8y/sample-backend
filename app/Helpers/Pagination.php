<?php

namespace App\Helpers;

class Pagination
{
    public static function paginate($data)
    {
        return [
            'current_page' => (integer)$data['current_page'],
            'first_page_url' => $data['first_page_url'] ?? '',
            'last_page_url' => $data['last_page_url'] ?? '',
            'next_page_url' => $data['next_page_url'] ?? '',
            'prev_page_url' => $data['prev_page_url'] ?? '',
            'path' => $data['path'] ?? '',
            'per_page' => (integer)$data['per_page'],
            'last_page' => $data['last_page'] ?? '',
            'from' => (integer)$data['from'],
            'to' => (integer)$data['to'],
            'total' => (integer)$data['total'],
        ];
    }
}
